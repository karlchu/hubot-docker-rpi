FROM karlchu/node-rpi:0.10.29

RUN apt-get update -y
RUN apt-get install -y redis-server
RUN ln -s `which nodejs` "$( dirname `which nodejs` )/node"
RUN npm install -g npm
RUN npm install -g hubot coffee-script yo generator-hubot

VOLUME /hubot

ENV BOT_NAME hubot
ENV BOT_ADAPTER shell
ENV BOT_UID 1000
ENV BOT_GID $BOT_UID

ENV PATH $PATH:/hubot/node_modules:/hubot/node_modules/hubot/node_modules

ENV CREATE_BOT_OWNER root@localhost.localdomain
ENV CREATE_BOT_DESCRIPTION 'Samplebot'

COPY contents/ /
RUN rm -f /etc/localtime /etc/timezone

CMD /start

