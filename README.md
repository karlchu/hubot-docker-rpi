
## To run a bot created before

```shell
$ docker run -d \
    -v /path/to/bot/on/host:/hubot \
    -e BOT_UID=$(id -u host_bot_user) \
    -e BOT_ADAPTER=slack \
    -e BOT_NAME='Bot Name' \
    karlchu/hubot-rpi:latest \
    /start
```

### Environemnts

* `BOT_UID` - Since `yo` does not like to run as the root user, hubot is run inside the container as a normal user.
  This specify the UID (and GID) this normal user should have.
  The `/path/to/bot/on/host` should be owned by the user with this UID.
* `BOT_NAME` - Name of the bot.
* `BOT_ADAPTER` - The adapter to use for the bot.

Depending on the adapter, you may also need to pass in extra environments for the adapter to work.
For example, the `slack` adapter requires the `HUBOT_SLACK_TOKEN`.

## To create a new bot

```shell
$ docker run --rm -it \
    -v /path/to/bot/on/host:/hubot \
    -e CREATE_BOT_OWNER='Steve Bot <steve@localhost.localdomain>' \
    -e CREATE_BOT_DESCRIPTION='My chat bot' \
    -e BOT_NAME=Steve \
    -e BOT_UID=$(id -u host_bot_user) \
    -e BOT_ADAPTER=slack \
    karlchu/hubot-rpi:latest \
    /create
```

This command will create a new hubot instance on the host at `/path/to/bot/on/host`.
